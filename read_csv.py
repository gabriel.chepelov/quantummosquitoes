import numpy as np
import pandas as pd


def csv_to_array(file): #converts a .csv file into a numpy array, keeps all lines and columns
    dataset = pd.read_csv(file, header=None)
    dataframes = pd.DataFrame(dataset)
    arr = np.array(dataframes.values)
    return arr



# importing infection_data

def infection_data_to_array(): #returns the interesting data of file data/infection_data.csv in a numpy array
    infection_data = 'data/infection_data.csv'
    raw_arr = csv_to_array(infection_data)
    n,p = len(raw_arr), len(raw_arr[0])
    arr = np.array( [ [ int(raw_arr[i][j]) for j in range(1,p) ] for i in range(1,n) ] )
    return arr


#importing data_2

def data_2_to_array():
    data_2 = 'data/data_2.csv'
    raw_arr = csv_to_array(data_2)
    n,p = len(raw_arr), len(raw_arr[0])
    arr = [ [0 for _ in range(1,p)] for _ in range(1,n) ]
    for i in range(1,n):
        for j in range(1,p):
            try:
                arr[i-1][j-1] = int(raw_arr[i][j])
            except:
                arr[i-1][j-1] = None
    
    return np.array(arr)


#importing data3

def data3_to_array():
    data3 = 'data/data3.csv'
    raw_arr = csv_to_array(data3)
    n,p = len(raw_arr), len(raw_arr[0])
    arr = [ [0 for _ in range(1,p)] for _ in range(1,n) ]
    for i in range(1,n):
        for j in range(1,p):
            try:
                arr[i-1][j-1] = int(raw_arr[i][j])
            except:
                arr[i-1][j-1] = None
    
    return np.array(arr)



# concatenating

def dead_birds_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][1]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][1] != None:
            arr2.append([data_2[i][0], data_2[i][1]])
    arr3 = np.array( [ [data3[i][0], data3[i][1]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))


def l_h1_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][2]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][9] != None:
            arr2.append([data_2[i][0], data_2[i][9]])
    arr3 = np.array( [ [data3[i][0], data3[i][7]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))


def c_h1_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][3]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][10] != None:
            arr2.append([data_2[i][0], data_2[i][10]])
    arr3 = np.array( [ [data3[i][0], data3[i][8]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))


def l_h2_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][4]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][12] != None:
            arr2.append([data_2[i][0], data_2[i][12]])
    arr3 = np.array( [ [data3[i][0], data3[i][10]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))


def c_h2_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][5]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][13] != None:
            arr2.append([data_2[i][0], data_2[i][13]])
    arr3 = np.array( [ [data3[i][0], data3[i][11]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))


def l_h3_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][6]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][15] != None:
            arr2.append([data_2[i][0], data_2[i][15]])
    arr3 = np.array( [ [data3[i][0], data3[i][13]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))


def c_h3_array(keepNone=False):
    inf_data = infection_data_to_array()
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr1 = np.array( [ [inf_data[i][0], inf_data[i][7]] for i in range(len(inf_data)) ] )
    arr2 = []
    for i in range(len(data_2)):
        if keepNone or data_2[i][16] != None:
            arr2.append([data_2[i][0], data_2[i][16]])
    arr3 = np.array( [ [data3[i][0], data3[i][14]] for i in range(len(data3)) ] )
    return np.concatenate((arr1, np.array(arr2), arr3))




def t_in_infection_data(): # returns a list of the t values existing in infection_data
    inf_data = infection_data_to_array()
    return np.array([ inf_data[i][0] for i in range(len(inf_data)) ])

def t_in_data3(): # idem for data3
    data3 = data3_to_array()
    return np.array([ data3[i][0] for i in range(len(data3)) ])


# new data from data2

def r_h1_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][11] != None:
            arr2.append([data_2[i][0], data_2[i][11]])
    arr3 = np.array( [ [data3[i][0], data3[i][9]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def r_h2_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][14] != None:
            arr2.append([data_2[i][0], data_2[i][14]])
    arr3 = np.array( [ [data3[i][0], data3[i][12]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def r_h3_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][17] != None:
            arr2.append([data_2[i][0], data_2[i][17]])
    arr3 = np.array( [ [data3[i][0], data3[i][15]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def l_b_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][2] != None:
            arr2.append([data_2[i][0], data_2[i][2]])
    arr3 = np.array( [ [data3[i][0], data3[i][2]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def n_m1_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][3] != None:
            arr2.append([data_2[i][0], data_2[i][3]])
    arr3 = np.array( [ [data3[i][0], data3[i][3]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def s_m1_array(keepNone=False):
    data_2 = data_2_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][4] != None:
            arr2.append([data_2[i][0], data_2[i][4]])
    if keepNone:
        for t in t_in_data3():
            arr2.append([t, None])
    return np.array(arr2)


def l_m1_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][5] != None:
            arr2.append([data_2[i][0], data_2[i][5]])
    arr3 = np.array( [ [data3[i][0], data3[i][4]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def n_m2_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][6] != None:
            arr2.append([data_2[i][0], data_2[i][6]])
    arr3 = np.array( [ [data3[i][0], data3[i][5]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))


def s_m2_array(keepNone=False):
    data_2 = data_2_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][7] != None:
            arr2.append([data_2[i][0], data_2[i][7]])
    if keepNone:
        for t in t_in_data3():
            arr2.append([t, None])
    return np.array(arr2)


def l_m2_array(keepNone=False):
    data_2 = data_2_to_array()
    data3 = data3_to_array()
    arr2 = []
    if keepNone:
        for t in t_in_infection_data():
            arr2.append([t, None])
    for i in range(len(data_2)):
        if keepNone or data_2[i][8] != None:
            arr2.append([data_2[i][0], data_2[i][8]])
    arr3 = np.array( [ [data3[i][0], data3[i][6]] for i in range(len(data3)) ] )
    return np.concatenate((np.array(arr2), arr3))
