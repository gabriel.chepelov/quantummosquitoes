# QuantumMosquitoes

This is the project of Group A for the EI Epidemiology.

## Files Organization

File final_model.ipynb includes the model and its simulation in its last version for the Saclay epidemic.

File west_nile.ipynb includes the model and its simulation in its final version for the Rennes epidemic.

File read_csv.py allows to read the raw data from the .csv files in the data folder.

Some deliverables are also available in the aRendre folder.


## Group Members

Jean Boillot

Gabriel Chepelov

Nathan Morin

Michaël Teboul
