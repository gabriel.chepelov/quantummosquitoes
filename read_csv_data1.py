import numpy as np
import pandas as pd


def csv_to_array(file): #converts a .csv file into a numpy array, keeps all lines and columns
    dataset = pd.read_csv(file, header=None)
    dataframes = pd.DataFrame(dataset)
    arr = np.array(dataframes.values)
    return arr


def infection_data_to_array(): #returns the interesting data of file data/infection_data.csv in a numpy array
    infection_data = 'data/infection_data.csv'
    raw_arr = csv_to_array(infection_data)
    n,p = len(raw_arr), len(raw_arr[0])
    arr = np.array( [ [ int(raw_arr[i][j]) for j in range(1,p) ] for i in range(1,n) ] )
    return arr


def dead_birds_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][1]] for i in range(len(inf_data)) ] )
    return arr


def l_h1_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][2]] for i in range(len(inf_data)) ] )
    return arr


def c_h1_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][3]] for i in range(len(inf_data)) ] )
    return arr


def l_h2_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][4]] for i in range(len(inf_data)) ] )
    return arr


def c_h2_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][5]] for i in range(len(inf_data)) ] )
    return arr


def l_h3_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][6]] for i in range(len(inf_data)) ] )
    return arr


def c_h3_array():
    inf_data = infection_data_to_array()
    arr = np.array( [ [inf_data[i][0], inf_data[i][7]] for i in range(len(inf_data)) ] )
    return arr
